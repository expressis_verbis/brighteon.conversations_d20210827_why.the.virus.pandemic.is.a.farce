# Subtitles to "Why the virus pandemic is a FARCE" (2021-08-27)

Subtitles to Brighteon Conversations: Why the virus pandemic is a FARCE.
Drs. Thomas Cowan and Andrew Kaufman interviewed by Mike Adams

(Please note that for the time being the German translation only covers the first half hour.)

* based on video: [Why the virus pandemic is a FARCE](https://www.brighteon.com/08e2ac4b-461e-4852-b8cf-2c2bded2a1ea)

* tools used: [ffmpeg.org](https://ffmpeg.org/), [alphacep/vosk-server](https://github.com/alphacep/vosk-server), brain and memory, and [dict.leo.org](https://dict.leo.org/englisch-deutsch/)
